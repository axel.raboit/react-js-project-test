import React, { useEffect } from 'react';
import './App.css';
import Header from './components/Header';
import styled from 'styled-components';
import Aos from 'aos';
import 'aos/dist/aos.css';

const Application = styled.div`
  background-color: #636e84;
  height: 100vh;
`

function App() {
  
  useEffect(() => {
    Aos.init({duration: 2000});
  }, []);

  return (
    <Application>
      <Header />
    </Application>
  );

 
}

export default App;
