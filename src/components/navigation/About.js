import React from 'react';
import styled from 'styled-components';

const ContainerTitle = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`

const Title = styled.h1`
    color: white;
`

export default function About() {
  return (
    <ContainerTitle>
        <Title data-aos="fade-down" data-aos-duration="1000">Welcome to the About page</Title>
    </ContainerTitle>
  );
}
