import React from 'react';
import styled from 'styled-components';

const Title = styled.h1`
    color: white;
`

export default function Marketing() {
    return (
        <Title data-aos="fade-up" data-aos-duration="1000">Nos service de Marketing</Title>
    );
}
